#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;
use crate::cors::CORS;
use db::{run_migrations, Db};
use rocket::fairing::AdHoc;

#[cfg(test)]
mod tests;

mod cors;
mod db;
mod resort;
mod user;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Db::fairing())
        .attach(AdHoc::on_ignite("Migrations", run_migrations))
        .attach(CORS)
        .attach(resort::stage())
        .attach(user::stage())
}
