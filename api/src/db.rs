use rocket::serde::{Deserialize, Serialize};
use rocket::{Build, Rocket};

#[database("resorts_db")]
pub struct Db(diesel::SqliteConnection);

pub async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    // This macro from `diesel_migrations` defines an `embedded_migrations`
    // module containing a function named `run` that runs the migrations in the
    // specified directory, initializing the database.
    embed_migrations!("db/migrations");

    let conn = Db::get_one(&rocket).await.expect("database connection");
    conn.run(|c| embedded_migrations::run(c))
        .await
        .expect("diesel migrations");

    rocket
}

#[derive(
    Debug, Clone, Deserialize, Serialize, Queryable, Insertable, Identifiable, AsChangeset,
)]
#[serde(crate = "rocket::serde")]
#[table_name = "resorts"]
pub struct Resort {
    #[serde(skip_deserializing)]
    id: Option<i32>,
    name: String,
    description: String,
    location: String,
    phone_number: String,
    open: bool,
}

table! {
    resorts (id) {
        id -> Nullable<Integer>,
        name -> Text,
        description -> Text,
        location -> Text,
        phone_number -> Text,
        open -> Bool,
    }
}

#[derive(
    Debug, Clone, Associations, Deserialize, Serialize, Queryable, Insertable, Identifiable,
)]
#[serde(crate = "rocket::serde")]
#[belongs_to(Resort, foreign_key = "favorite_place")]
#[table_name = "users"]
pub struct User {
    #[serde(skip_deserializing)]
    id: Option<i32>,
    first_name: String,
    last_name: String,
    favorite_place: i32,
}

table! {
    users (id) {
        id -> Nullable<Integer>,
        first_name -> Text,
        last_name -> Text,
        favorite_place -> Integer,
    }
}
