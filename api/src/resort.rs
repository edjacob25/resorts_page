use rocket::fairing::AdHoc;
use rocket::response::{status::Created, Debug};
use rocket::serde::json::Json;

use rocket_sync_db_pools::diesel;

use self::diesel::prelude::*;

use crate::db::{resorts, Db, Resort};

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

#[post("/", data = "<resort>")]
async fn create(db: Db, resort: Json<Resort>) -> Result<Created<Json<Resort>>> {
    let resort_value = resort.clone();
    db.run(move |conn| {
        diesel::insert_into(resorts::table)
            .values(resort_value)
            .execute(conn)
    })
    .await?;

    Ok(Created::new("/").body(resort))
}

#[get("/")]
async fn list(db: Db) -> Result<Json<Vec<Option<i32>>>> {
    let ids: Vec<Option<i32>> = db
        .run(move |conn| resorts::table.select(resorts::id).load(conn))
        .await?;

    Ok(Json(ids))
}

#[get("/<id>")]
async fn read(db: Db, id: i32) -> Option<Json<Resort>> {
    db.run(move |conn| resorts::table.filter(resorts::id.eq(id)).first(conn))
        .await
        .map(Json)
        .ok()
}

#[put("/<id>", data = "<resort>")]
async fn update(db: Db, id: i32, resort: Json<Resort>) -> Result<Json<Resort>> {
    let resort_value = resort.clone();
    db.run(move |conn| {
        diesel::update(resorts::table.find(id))
            .set(resort_value)
            .execute(conn)
    })
    .await?;

    Ok(resort)
}

#[delete("/<id>")]
async fn delete(db: Db, id: i32) -> Result<Option<()>> {
    let affected = db
        .run(move |conn| {
            diesel::delete(resorts::table)
                .filter(resorts::id.eq(id))
                .execute(conn)
        })
        .await?;

    Ok((affected == 1).then(|| ()))
}

#[delete("/")]
async fn destroy(db: Db) -> Result<()> {
    db.run(move |conn| diesel::delete(resorts::table).execute(conn))
        .await?;

    Ok(())
}

#[options("/")]
async fn options() -> Result<()> {
    Ok(())
}

#[options("/<id>")]
async fn options_dyn(id: Option<i32>) -> Result<()> {
    Ok(())
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Resorts Stage", |rocket| async {
        rocket.mount(
            "/resorts",
            routes![list, read, create, update, delete, destroy, options, options_dyn],
        )
    })
}
