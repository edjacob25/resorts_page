use crate::db::{run_migrations, Db};
use rocket::fairing::AdHoc;
use rocket::http::Status;
use rocket::local::blocking::Client;
use rocket::serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
struct Place {
    name: String,
    description: String,
    location: String,
    phone_number: String,
    open: bool,
}

fn test(base: &str, stage: AdHoc) {
    const N: usize = 20;

    let client = Client::tracked(
        rocket::build()
            .attach(Db::fairing())
            .attach(AdHoc::on_ignite("Migrations", run_migrations))
            .attach(stage),
    )
    .unwrap();

    // Clear everything from the table.
    assert_eq!(client.delete(base).dispatch().status(), Status::Ok);
    assert_eq!(
        client.get(base).dispatch().into_json::<Vec<i64>>(),
        Some(vec![])
    );

    // Add some random places, ensure they're listable and readable.
    for i in 1..=N {
        let name = format!("The place {}", i);
        let description = format!("A beautiful place which spawns {} kilometers", i);
        let location = format!("Located at the {} mountain", i);
        let phone_number = format!("{}", i);
        let place = Place {
            name: name.clone(),
            description: description.clone(),
            location: location.clone(),
            phone_number: phone_number.clone(),
            open: true,
        };

        // Create a new places.
        let response = client
            .post(base)
            .json(&place)
            .dispatch()
            .into_json::<Place>();
        assert_eq!(response.unwrap(), place);

        // Ensure the index shows one more place.
        let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
        assert_eq!(list.len(), i);

        // The last in the index is the new one; ensure contents match.
        let last = list.last().unwrap();
        let response = client.get(format!("{}/{}", base, last)).dispatch();
        assert_eq!(response.into_json::<Place>().unwrap(), place);
    }

    // Now delete all of the places.
    for _ in 1..=N {
        // Get a valid ID from the index.
        let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
        let id = list.get(0).expect("have post");

        // Delete that place.
        let response = client.delete(format!("{}/{}", base, id)).dispatch();
        assert_eq!(response.status(), Status::Ok);
    }

    // Ensure they're all gone.
    let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
    assert!(list.is_empty());

    // Trying to delete should now 404.
    let response = client.delete(format!("{}/{}", base, 1)).dispatch();
    assert_eq!(response.status(), Status::NotFound);
}

#[test]
fn test_resorts() {
    test("/resorts", crate::resort::stage())
}
