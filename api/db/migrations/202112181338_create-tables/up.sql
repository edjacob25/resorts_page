CREATE TABLE resorts (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    location VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    open BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    favorite_place integer NOT NULL,
    FOREIGN KEY (favorite_place) REFERENCES resorts(id)
);