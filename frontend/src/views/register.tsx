import React from "react";
import axios from 'axios';
import configData from "../config.json";
import {useNavigate} from "react-router-dom";
import ErrorPanel from "../components/errorPanel";

type RegisterState = {
    places: Array<[number, string]>,
    error: boolean,
    errorText: string | null,
    firstName: string,
    lastName: string,
    favoritePlace: number | null
}

interface RegisterProps {
    onSuccessfulRegister(firstName: string): void
}

interface RegisterPropsComplete {
    onSuccessfulRegister(firstName: string): void

    navigate(s: string, p: { replace: boolean }): void
}

class RegisterNavigated extends React.Component<RegisterPropsComplete, RegisterState> {

    state: RegisterState = {
        places: [],
        error: false,
        errorText: null,
        firstName: "",
        lastName: "",
        favoritePlace: null
    };

    componentDidMount() {
        let resortsUrl = `${configData.api_url}/resorts`;
        axios.get<number[]>(resortsUrl)
            .then(res => {
                for (let id of res.data) {
                    axios.get(resortsUrl + "/" + id).then(place => {

                        this.setState({places: this.state.places.concat([[id, place.data.name]])});
                    }).catch(_ => {
                            this.setState({error: true, errorText: "Network error, please try again"})
                        }
                    )
                }
                if (res.data.length > 0) {
                    this.setState({favoritePlace: res.data[0]})
                }

            }).catch(_ => {
                this.setState({error: true, errorText: "Network error, please try again"})
            }
        );

    }

    handleChange = (event: React.FormEvent<HTMLInputElement> | React.FormEvent<HTMLSelectElement>) => {
        const value = event.currentTarget.value;

        this.setState({

            ...this.state,

            [event.currentTarget.name]: value

        });
    }

    handleSubmit = (event: React.FormEvent) => {
        event.preventDefault();

        this.setState({error: false})
        if (this.state.favoritePlace === null || this.state.firstName.length === 0 || this.state.lastName.length === 0) {
            this.setState({error: true, errorText: "One of the fields is not complete, please adjust and try again"})
            return
        }


        axios.post(`${configData.api_url}/users`, {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            favorite_place: this.state.favoritePlace
        }).then(response => {
            if (response.status === 201) {
                this.props.onSuccessfulRegister(this.state.firstName);
                this.props.navigate("/", {replace: true})
            }
        }).catch(e => {
            this.setState({error: true, errorText: "There was an error with the request, please try again"})
        })
    }

    render() {
        return <>
            <h2 className="title">Register with us</h2>
            <form onSubmit={this.handleSubmit}>
                <div className="field">
                    <label className="label">First name</label>
                    <div className="control">
                        <input className="input" value={this.state.firstName} type="text" name="firstName"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Last name</label>
                    <div className="control">
                        <input className="input" value={this.state.lastName} type="text" name="lastName"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Favorite place</label>
                    <div className="control">
                        <div className="select">
                            <select name="favoritePlace" value={this.state.favoritePlace ?? undefined}
                                    onChange={this.handleChange}>
                                {
                                    this.state.places.map(place =>
                                        <option key={place[0]} value={place[0]}>
                                            {place[1]}
                                        </option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                </div>

                <div className="field">
                    <div className="control">
                        <button className="button is-link">Submit</button>
                    </div>
                </div>
            </form>
            <ErrorPanel isError={this.state.error} text={this.state.errorText ?? ""}/>
        </>
    }
}

function Register(props: RegisterProps) {
    let navigate = useNavigate();
    return <RegisterNavigated onSuccessfulRegister={props.onSuccessfulRegister} navigate={navigate}/>
}

export default Register;