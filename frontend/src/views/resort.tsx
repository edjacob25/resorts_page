import React from 'react';
import {useParams} from "react-router-dom";
import ErrorPanel from "../components/errorPanel";
import axios from "axios";
import Resort from "../models/Resort";
import SuccessPanel from "../components/successPanel";
import configData from "../config.json";

type ResortProps = {
    id: number
}

type ResortState = {
    name: string,
    description: string,
    location: string,
    phone_number: string,
    open: boolean,
    error: boolean, errorText: string | null,
    loaded: boolean, updated: boolean
}


class ResortEditable extends React.Component<ResortProps, ResortState> {

    state: ResortState = {
        name: "", description: "", location: "", phone_number: "", open: false,
        error: false, errorText: null, loaded: false, updated: false
    }

    componentDidMount() {
        let resortsUrl = `${configData.api_url}/resorts`;

        axios.get<Resort>(`${resortsUrl}/${this.props.id}`).then(res => {
            let place = res.data;
            this.setState({
                name: place.name,
                description: place.description,
                location: place.location,
                phone_number: place.phone_number,
                open: place.open,
                loaded: true
            });
        }).catch(_ => {
                this.setState({error: true, errorText: "Network error, please try again"})
            }
        )
    }

    componentDidUpdate(prevProps: Readonly<ResortProps>, prevState: Readonly<ResortState>, snapshot?: any) {
        if (prevProps.id !== this.props.id) {
            let resortsUrl = `${configData.api_url}/resorts`;
            this.setState({updated: false})
            axios.get<Resort>(`${resortsUrl}/${this.props.id}`).then(res => {
                let place = res.data;
                this.setState({
                    name: place.name,
                    description: place.description,
                    location: place.location,
                    phone_number: place.phone_number,
                    open: place.open,
                    loaded: true
                });

            }).catch(_ => {
                    this.setState({error: true, errorText: "Network error, please try again"})
                }
            )
        }
    }

    handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const value = event.currentTarget.value;

        this.setState({

            ...this.state,

            [event.currentTarget.name]: value

        });
    }

    handleSubmit = (event: React.FormEvent) => {
        event.preventDefault();

        this.setState({error: false})
        let place = this.state;
        if (place.name.length === 0 || place.description.length === 0 || place.location.length === 0) {
            this.setState({error: true, errorText: "One of the fields is not complete, please adjust and try again"})
            return
        }


        axios.put(`${configData.api_url}/resorts/${this.props.id}`, place).then(response => {
            if (response.status === 200) {
                this.setState({updated: true})
            }
        }).catch(e => {
            this.setState({error: true, errorText: "There was an error with the request, please try again"})
        })
    }

    render() {
        return <>
            <h2 className="title">Register with us</h2>
            <form onSubmit={this.handleSubmit}>
                <div className="field">
                    <label className="label">Name</label>
                    <div className="control">
                        <input className="input" value={this.state.name} type="text" name="name"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                        <input className="input" value={this.state.description} type="text" name="description"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Location</label>
                    <div className="control">
                        <input className="input" value={this.state.location} type="text" name="location"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <label className="label">Phone number</label>
                    <div className="control">
                        <input className="input" value={this.state.phone_number} type="text" name="phone_number"
                               onChange={this.handleChange}/>
                    </div>
                </div>

                <div className="field">
                    <div className="control">
                        <button className="button is-link">Submit</button>
                    </div>
                </div>
            </form>
            <ErrorPanel isError={this.state.error} text={this.state.errorText ?? ""}/>

            <SuccessPanel isSuccess={this.state.updated} text={`The record ${this.props.id} has been updated`}/>
        </>
    }
}

function ResortView() {
    let params = useParams();
    return (
        <ResortEditable id={parseInt(params.resortId ?? "0")}/>
    );
}

export default ResortView;