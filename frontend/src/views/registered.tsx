import React from 'react';
import axios from "axios";
import PersonRow from "../components/userRow";
import Person from "../models/Person";
import configData from "../config.json";

type PersonJson = {
    first_name: string,
    last_name: string,
    favorite_place: number,
    id: number
}


type RegisteredPersonsState = {
    persons: Person[],
    error: boolean,
    errorText: string | null,
    loading: boolean
}

class Registered extends React.Component<any, RegisteredPersonsState> {

    state: RegisteredPersonsState = {
        persons: [], error: false, errorText: null, loading: true
    }

    componentDidMount() {
        let personsUrl = `${configData.api_url}/users`;
        let resortsUrl = `${configData.api_url}/resorts`;
        axios.get<number[]>(personsUrl)
            .then(res => {
                for (let id of res.data) {
                    axios.get<PersonJson>(personsUrl + "/" + id).then(person => {
                        axios.get(resortsUrl + "/" + person.data.favorite_place).then(place => {
                            this.setState({
                                persons: this.state.persons.concat({
                                    id: person.data.id,
                                    firstName: person.data.first_name,
                                    lastName: person.data.last_name,
                                    favoritePlace: place.data.name
                                })
                            });
                        })
                    })
                        .catch(_ => {
                                this.setState({error: true, errorText: "Network error, please try again"})
                            }
                        )
                }

            })
            .catch(_ => {
                this.setState({error: true, errorText: "Network error, please try again"})
            })
            .finally(() =>
                this.setState({
                    loading: false
                })
            );

    }

    render() {
        if (this.state.loading) {
            return <div className="title">
                Loading, please wait
            </div>
        } else {
            let a = this.state.persons.map(p => {
                return <PersonRow key={p.id} person={p}/>
            })
            return <>
                {a}
            </>

        }
    }
}

export default Registered;