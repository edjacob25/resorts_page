import React from 'react';
import Resort from "../models/Resort";
import axios from "axios";
import {Link, Outlet} from "react-router-dom";
import configData from "../config.json";

type ResortsState = {
    resorts: Resort[],
    error: boolean,
    errorText: string | null
}


class Resorts extends React.Component<any, any> {

    state: ResortsState = {
        resorts: [],
        error: false,
        errorText: null
    }


    componentDidMount() {
        let resortsUrl = `${configData.api_url}/resorts`;
        axios.get<number[]>(resortsUrl)
            .then(res => {
                for (let id of res.data) {
                    axios.get<Resort>(resortsUrl + "/" + id).then(place => {
                        this.setState({resorts: this.state.resorts.concat(place.data)});
                    }).catch(_ => {
                            this.setState({error: true, errorText: "Network error, please try again"})
                        }
                    )
                }
            }).catch(_ => {
                this.setState({error: true, errorText: "Network error, please try again"})
            }
        );
    }


    render() {
        let resortsView = this.state.resorts.map(r => {
            return <li key={r.id} className="column is-3">
                <Link to={`/resorts/${r.id}`}>{r.name} </Link>
            </li>
        });
        return (<>
                <ul className="section columns is-multiline">
                    {resortsView}
                </ul>
                <Outlet/>
            </>
        )
    }
}

export default Resorts;