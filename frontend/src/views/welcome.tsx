import React from 'react';

function Welcome(props: { firstName: string }) {
    return (
        <div className="hero is-primary">
            <div className="hero-body">
                <p className="title">
                    Thank you, {props.firstName}
                </p>
                <p className="subtitle">
                    Welcome to the ski webpage
                </p>
            </div>
        </div>
    );
}

export default Welcome;