import React from 'react';
import {BrowserRouter, Route, Routes,} from "react-router-dom";
import 'bulma/css/bulma.min.css';
import './App.css';
import Layout from "./components/layout";
import Register from "./views/register";
import Welcome from "./views/welcome";
import {PrivateRoute} from "./PrivateRoutes";
import Registered from "./views/registered";
import Resort from "./views/resort";
import Resorts from "./views/resorts";

type AppState = {
    isLoggedIn: boolean,
    firstName: string | null
};


class App extends React.Component<any, AppState> {

    state: AppState = {
        isLoggedIn: false,
        firstName: null
    };

    onSuccessfulRegister = (firstName: string) => {
        this.setState({isLoggedIn: true, firstName: firstName})
    }

    render() {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout isLogged={this.state.isLoggedIn}/>}>
                        <Route path="/register" element={<Register onSuccessfulRegister={this.onSuccessfulRegister}/>}/>
                        <Route path="/" element={
                            <PrivateRoute isLogged={this.state.isLoggedIn}>
                                <Welcome firstName={this.state.firstName ?? ""}/>
                            </PrivateRoute>
                        }/>

                        <Route path="/registered" element={
                            <PrivateRoute isLogged={this.state.isLoggedIn}>
                                <Registered/>
                            </PrivateRoute>
                        }/>

                        <Route path="resorts" element={
                            <PrivateRoute isLogged={this.state.isLoggedIn}>
                                <Resorts/>
                            </PrivateRoute>
                        }>
                            <Route path=":resortId" element={
                                <PrivateRoute isLogged={this.state.isLoggedIn}>
                                    <Resort/>
                                </PrivateRoute>
                            }/>
                        </Route>

                        <Route
                            path="*"
                            element={
                                <main style={{padding: "1rem"}}>
                                    <p>There's nothing here!</p>
                                </main>
                            }
                        />

                    </Route>
                </Routes>
            </BrowserRouter>
        );
    }
}

export default App;
