type Person = {
    firstName: string,
    lastName: string,
    favoritePlace: string,
    id: number
}

export default Person