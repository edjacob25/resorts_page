type Resort = {
    id: number,
    name: string,
    description: string,
    location: string,
    phone_number: string,
    open: boolean
}

export default Resort