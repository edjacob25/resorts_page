import React from 'react';
import {Navigate} from 'react-router-dom';

export const PrivateRoute = (props: { children: JSX.Element, isLogged: boolean }) => {

    return props.isLogged ? props.children : <Navigate to="/register"/>;
}

