import React from "react";


function SuccessPanel(props: { isSuccess: boolean, text: string }) {
    return props.isSuccess ? <div className="message is-success">
        <div className="message-header">
            <p>Record updated</p>
        </div>
        <div className="message-body">
            <p>{props.text}</p>
        </div>
    </div> : <></>
}

export default SuccessPanel;