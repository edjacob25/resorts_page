import React from 'react';
import Person from "../models/Person";

function PersonRow(props: { person: Person }) {
    return <div className="card">
        <div className="card-content">
            <p className="title is-4">
                {props.person.firstName} {props.person.lastName}
            </p>
            <p className="subtitle is-6">
                Their favorite place is {props.person.favoritePlace}
            </p>
        </div>
    </div>
}

export default PersonRow