import React from "react";


function ErrorPanel(props: { isError: boolean, text: string }) {
    return props.isError ? <div className="message is-danger">
        <div className="message-header">
            <p>Error</p>
        </div>
        <div className="message-body">
            <p>{props.text}</p>
        </div>
    </div> : <></>
}

export default ErrorPanel;