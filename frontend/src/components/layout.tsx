import React from 'react';
import {Link, Outlet} from "react-router-dom";

function Layout(props: { isLogged: boolean }) {
    let nav = props.isLogged ? <>
        <Link className="navbar-item" to="/">Home</Link>
        <Link className="navbar-item" to="/resorts">Resorts</Link>
        <Link className="navbar-item" to="/registered">Registered people</Link>
    </> : <Link className="navbar-item is-active" to="/register">Register</Link>

    return (<>
        <header className="navbar is-dark">
            <div className="navbar-brand">
                <h1 className="navbar-brand navbar-item">Resort manager</h1>
                <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false"
                   data-target="navbarBasicExample">
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                    <span aria-hidden="true"/>
                </a>
            </div>
            <nav className="navbar-menu">
                <div className="navbar-start">
                    {nav}
                </div>
            </nav>

        </header>
        <div className="container">
            <Outlet/>
        </div>
    </>);
}

export default Layout;