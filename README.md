# Ski Api and website

This is a project which tests the use of rust with the Rocket framework as a REST api for a React frontend.

## Running it

### Api
After cloning, just `cd` into the `api` directory and then `cargo run`. By deafult it will run in port 8000

### Frontend
After cloning, `cd` into the `frontend` directory. Then:

```sh
npm install
npm start
```

By default, it will ook for the api in `127.0.0.1:8000`, so if you cahnge it, adjust accordingly in `config.json`